export function addProductToCart(id){
    return {
        type: "ADD_PRODUCT_TO_CART",
        payload: {
            id: id
        }
    }
}

export function IncreaseQuantity(id){
    return{
        type: "INCREASE_QUANTITY",
        payload: {
            id: id
        }
    }
}

export function DecreaseQuantity(id){
    return{
        type: "DECREASE_QUANTITY",
        payload: {
            id: id
        }
    }
}

export function RemoveItemFromCart(id){
    return {
        type: "REMOVE_ITEM_FROM_CART",
        payload: {
            id: id
        }
    }
}

export function EmptyCart(){
    return {
        type: "EMPTY_CART"
    }
}

export function AddUserInfo(FirstName, LastName, Mobile, Email){
    return {
        type: "ADD_USER_INFO",
        payload: {
            Name: FirstName+" "+LastName,
            Mobile: Mobile,
            Email: Email
        }
    }
}

export function AddUserAddress(FirstName, LastName, Mobile, Email, Address, City, State, PinCode){
    return {
        type: "ADD_USER_ADDRESS",
        payload: {
            Name: FirstName+" "+LastName,
            Mobile: Mobile,
            Email: Email,
            Address: Address,
            City: City,
            State: State,
            PinCode: PinCode,
        }
    }
}

export function RemoveAddress(){
    return {
        type: "REMOVE_ADDRESS"
    }
}

export function AddProductsToOrders(products){
    return {
        type: "ADD_PRODUCTS_TO_ORDERS",
        payload: {
            products: products
        }
    }
}

export function AddItemsToHistory(products){
    return {
        type: "ADD_ITEMS_TO_HISTORY",
        payload: {
            products: products
        }
    }
}

export function emptyOrderedItems(){
    return {
        type: "EMPTY_ORDERED_ITEMS",
    }
}
