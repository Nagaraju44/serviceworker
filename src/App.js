import React from 'react';
import './App.css';
import Header from './components/Header';
import Home from './components/Home/Home';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import DisplayProducts from './components/DisplayProducts';
import ViewProduct from './components/viewProduct/viewProduct';
import Cartpage from './components/cartpage/cartpage';
import Form from './components/Form';
import Checkout from './components/Checkout/checkout';
import Orderplaced from './components/orderplaced/orderplaced';
import MyOrders from './components/MyOrders/MyOrders';
import NotFound from './components/NotFound/NotFound';

function App() {
  return (
    <div className="App pb-5">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route exact path='/' element={<Home />} />
          <Route exact path='/displayproducts' element={<DisplayProducts />} />
          <Route exact path='/cartpage' element={<Cartpage />} />
          <Route exact path='/viewproduct/:id' element={<ViewProduct />} />
          <Route exact path='/loginForm' element={<Form />} />
          <Route exact path='/checkout' element={<Checkout />} />
          <Route exact path='/orderStatus' element={<Orderplaced />} />
          <Route exact path='/myorders' element={<MyOrders />} />
          <Route exact path='*' element={<NotFound />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
