import React, { Component } from 'react'

export class NotFound extends Component {
  render() {
    return (
      <div className='container'>
        <div className='row'>
            <div className='col-12 mt-5'>
                <img src='https://web.abijita.com/wp-content/uploads/2018/02/404-Error.png' className='w-50' alt='notFound' />
            </div>
        </div>
      </div>
    )
  }
}

export default NotFound