import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { IncreaseQuantity } from '../../Redux/Actions/Actions'
import { DecreaseQuantity } from '../../Redux/Actions/Actions'
import { RemoveItemFromCart } from '../../Redux/Actions/Actions'
import { emptyOrderedItems } from '../../Redux/Actions/Actions'

function name(params) {
    
}


class Cartpage extends Component {
    FuncIncreaseQuantity = (event) => {
        this.props.IncreaseQuantity(event.target.id)
    }

    FuncDecreaseQuantity = (event) => {
        let requiredItem = this.props.cartItems.filter((item) => {
            return item.id == event.target.id
        })[0]

        if (requiredItem.number > 1) {
            this.props.DecreaseQuantity(event.target.id)
        } else {
            this.setState({ isAddedToCart: false })
            this.props.RemoveItemFromCart(event.target.id)
        }
    }

    getQuantity = (id) => {
        let product = this.props.cartItems.filter((item) => {
            return item.id == id
        })
        return product[0].number
    }

    TotalCostOfProducts = (id) => {
        let product = this.props.cartItems.filter((item) => {
            return item.id == id
        })
        return parseInt(product[0].number) * parseInt(product[0].price)
    }

    GrossBill = () => {
        let totalBill = this.props.cartItems.reduce((price, product) => {
            price = price + parseInt(product.price) * parseInt(product.number)
            return price
        }, 0)
        return totalBill
    }
    EmptyOrderdItems = () => {
        this.props.emptyOrderedItems()
    }

    render() {
        if (this.props.cartItems.length > 0) {
            return (
                <div className='container-fluid'>
                    <div className='row mt-4'>
                        <div className='col-12 text-start'>
                            <h3 className='ps-md-4 ms-md-3'><strong>Your Cart</strong></h3>
                            <hr />
                        </div>
                        {this.props.cartItems.map((item) => {
                            let requiredProduct = this.props.ProductsData.filter((product) => {
                                return product.id == item.id
                            })

                            return (<div className="col-12 d-flex justify-content-start mb-3 d-flex flex-wrap border border-secondary border-opacity-25" key={requiredProduct[0].id}>
                                <div className='col-md-3 col-5 order-1 order-md-1 d-flex align-items-center justify-content-center'>
                                    <img className='w-50' src={requiredProduct[0].img} alt='product' />
                                </div>
                                <div className='col-md-3 col-5 order-4 order-md-2 mt-3 mt-md-0  d-flex flex-column justify-content-center text-wrap text-start ps-4'>
                                    <h4 className='fs-6'>{requiredProduct[0].title}</h4>
                                    <p className='d-none d-md-block'><strong>Category: </strong>{requiredProduct[0].category}</p>
                                    <div className='col-12 col-md-9 col-lg-8'>
                                        <h4 className='border border-1 w-100 text-center d-flex flex-nowrap align-items-center justify-content-center'>Qty:
                                            <button className='border border-0 mx-lg-2 me-2 w-25 btn btn-danger rounded-0' id={requiredProduct[0].id} onClick={this.FuncDecreaseQuantity}>-</button>
                                            {this.getQuantity(requiredProduct[0].id)}
                                            <button className='border border-0 mx-lg-2 ms-2 w-25 btn btn-success rounded-0' id={requiredProduct[0].id} onClick={this.FuncIncreaseQuantity}>+</button>
                                        </h4>
                                    </div>
                                </div>
                                <div className='col-md-3 col-3 order-2 order-md-3 d-flex flex-column justify-content-md-center justify-content-end'>
                                    <h3 className='fs-6'><strong>Price:</strong></h3>
                                    <h2 className='text-success'><strong>&#8377;{requiredProduct[0].price}</strong></h2>
                                </div>
                                <div className='col-md-3 col-3 order-3 order-md-4 d-flex flex-column justify-content-md-center justify-content-end'>
                                    <h3 className='fs-6'><strong>Total:</strong></h3>
                                    <h2 className='text-success'><strong>&#8377;{this.TotalCostOfProducts(requiredProduct[0].id)}</strong></h2>
                                </div>
                            </div>)
                        })}
                        <div className='col-lg-9 col-md-8 col-6'>

                        </div>
                        <div className='col-5 col-md-4 col-lg-3'>
                            <h3 className='text-center text-nowrap fs-6'><strong>Deliver Charges: <span className='text-success'>Free</span></strong></h3>
                        </div>
                        <div className='col-12 text-start ps-md-5 ps-1'>
                            <Link to='/displayproducts'>
                                <button className='btn text-light' style={{ backgroundColor: '#84c225' }}><strong>&#43; Add More</strong></button>
                            </Link>
                        </div>
                        <div className='col-12 mt-md-5 mt-3'>
                            <h3 className='text-start ps-md-4 ps-0 ms-md-3 ms-0'><strong>Your Total Bill</strong></h3>
                            <hr />
                        </div>
                        <div className='col-7'>
                        </div>
                        <div className='col-4 text-md-end'>
                            <h3 className='pe-4 pe-md-0 text-nowrap'><strong>Total: <span className='text-success fs-3'>&#8377;{this.GrossBill()}</span></strong></h3>
                            <Link to='/loginForm'>
                                <button className='btn btn-danger px-4' onClick={this.EmptyOrderdItems}><strong>Buy Now</strong></button>
                            </Link>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className='container'>
                    <div className='row'>
                        <div className='col-12 mt-5 pt-5'>
                            <div className='col-12 col-md-8 col-lg-6 mx-auto'>
                                <img src='https://www.kindpng.com/picc/m/174-1749396_empty-cart-your-cart-is-empty-hd-png.png' className='w-50 ' alt='cartEmpty' />
                            </div>
                            <h3 className='mt-3'>You Have No Products in Cart</h3>
                            <Link to='/displayproducts'>
                                <button className='btn btn-success text-light border border-0' style={{ backgroundColor: '#4c6020' }}>More Products</button>
                            </Link>
                        </div>
                    </div>
                </div>
            )
        }

    }
}

const mapStateToProps = (state) => {
    return {
        ProductsData: state.ProductsData,
        cartItems: state.cartItems
    }
}

const mapDispatchToProps = {
    IncreaseQuantity,
    DecreaseQuantity,
    RemoveItemFromCart,
    emptyOrderedItems
}


export default connect(mapStateToProps, mapDispatchToProps)(Cartpage)