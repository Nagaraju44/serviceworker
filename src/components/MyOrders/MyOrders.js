import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

export class MyOrders extends Component {

    getQuantity = (id) => {
        let product = this.props.orderedHistory.filter((item) => {
            return item.id == id
        })
        return product[0].number
    }

    TotalCostOfProducts = (id) => {
        let product = this.props.orderedHistory.filter((item) => {
            return item.id == id
        })
        return parseInt(product[0].number) * parseInt(product[0].price)
    }

    render() {
        if (this.props.orderedHistory.length <= 0) {
            return <div className='container'>
                <div className='row'>
                    <div className='col-12 mt-5'>
                        <h3 className='text-start'><strong>My Orders</strong></h3>
                        <p className='text-start mb-5'>Showing orders for the last 6 months</p>
                        <hr />
                    </div>
                    <div className='col-12'>
                        <img src='https://cdn1.iconfinder.com/data/icons/business-and-marketing-16/128/policy-512.png' alt='noHistory' className='w-25 mt-5' />
                        <h3 className='mt-4'>No Orders Found From the Past 6 Months</h3>
                        <Link to='/displayproducts'>
                            <button className='btn btn-success text-light border border-0' style={{ backgroundColor: '#4c6020' }} onClick={this.clearOrders}>
                                <strong><i className="fa-solid fa-basket-shopping text-light fs-4 pe-2"></i>Explore Products</strong>
                            </button>
                        </Link>
                    </div>
                </div>
            </div>
        }
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-12 mt-5'>
                        <h3 className='text-start'><strong>My Orders</strong></h3>
                        <p className='text-start mb-md-5 mb-3'>Showing orders for the last 6 months</p>
                        <hr />
                        {this.props.orderedHistory.reverse().map((product) => {
                            return (<div className="col-12 d-flex justify-content-start mb-5 d-flex flex-wrap" key={product.id + 'x'}>
                                <div className='col-md-3 col-5 order-1 order-md-1 d-flex align-items-center justify-content-center'>
                                    <img className='w-50' src={product.img} alt='product' />
                                </div>
                                <div className='col-md-3 col-8 order-4 order-md-2 mt-3 mt-md-0  d-flex flex-column justify-content-center text-wrap text-start ps-4'>
                                    <h4 className='fs-6'>{product.title}</h4>
                                    <p className='d-none d-lg-block'><strong>Category: </strong>{product.category}</p>
                                    <h4 className='border border-1 w-75 text-center d-flex flex-nowrap align-items-center justify-content-center'>Qty: {this.getQuantity(product.id)}</h4>
                                </div>
                                <div className='col-md-3 col-3 order-2 order-md-3 d-flex flex-column justify-content-md-center justify-content-end'>
                                    <h3 className='fs-5'><strong>Price:</strong></h3>
                                    <h2 className='text-success fs-5'><strong>&#8377;{product.price}</strong></h2>
                                </div>
                                <div className='col-md-3 col-3 order-3 order-md-4 d-flex flex-column justify-content-md-center justify-content-end'>
                                    <h3 className='fs-5'><strong>Total:</strong></h3>
                                    <h2 className='text-success fs-5'><strong>&#8377;{this.TotalCostOfProducts(product.id)}</strong></h2>
                                </div>
                            </div>)
                        })}
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        orderedHistory: state.orderedHistory
    }
}

export default connect(mapStateToProps, null)(MyOrders)