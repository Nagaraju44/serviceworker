import React, { Component } from 'react'
import Offers from '../offers';
import SaleAndBankOffer from '../saleAndBankOffer'
import MoreProducts from '../MoreProducts'
import Footer from '../Footer';
import Courosel from '../courosel/courosel';

export class Home extends Component {
    render() {
        return (
            <div>
                <Courosel />
                <Offers />
                {/* <SaleAndBankOffer /> */}
                <MoreProducts />
                <Footer />
            </div>
        )
    }
}

export default Home