import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './index.css'
import { connect } from 'react-redux'
import { addProductToCart } from '../../Redux/Actions/Actions'

export class Offers extends Component {
    render() {
        return (
            <div className='container'>
                <div className='row mt-5'>
                    <div className='col-lg-2 col-md-4 col-6 mb-3'>
                        <button className='btn btn-secondary opacity-50 me-3 h-100 w-100 fw-bold' >EGGS, MEAT <br />AND FISH</button>
                    </div>
                    <div className='col-lg-2 col-md-4 col-6 mb-3'>
                        <div className='bg-dark rounded w-100 h-100'><img src="https://www.tajhotels.com/content/dam/tic/tataneu/logo/neupass-logo-white-transparent.png/jcr:content/renditions/cq5dam.web.323.323.png" className='w-100 h-100' /></div>
                    </div>
                    <div className='col-lg-2 col-md-4 col-6 mb-3'>
                        <button className='btn btn-success h-100 w-100 fw-bold' style={{ backgroundColor: '#4c6020' }}>AYURVEDA</button>
                    </div>
                    <div className='col-lg-2 col-md-4 col-6 mb-3'>
                        <button className='btn btn-secondary opacity-50 h-100 w-100 fw-bold'>BUY MORE <br />SAVE MORE</button>
                    </div>

                    <div className='col-lg-2 col-md-4 col-6'>
                        <button className='btn btn-secondary opacity-50 h-100 w-100 fw-bold'>DEALS OF <br />THE WEEK</button>
                    </div>

                    <div className='col-lg-2 col-md-4 col-6 '>
                        <button className='btn btn-secondary opacity-50 h-100 w-100 fw-bold'>COMBO STORE</button>
                    </div>

                    <div className='col-12 d-flex flex-row justify-content-between pt-3 shadow-sm mt-4'>
                        <h3 className='text-dark mx-auto'>My Smart Basket</h3>
                        <Link to='/displayproducts'>
                            <button className='btn btn-success btn-sm float-end border border-0' style={{ backgroundColor: '#84c225' }}>ShowMore</button>
                        </Link>
                    </div>

                    <div className='col-lg-3 col-md-4 col-6 border border-secondary border-opacity-25 rounded pb-2'>
                        <Link to={`/viewproduct/1`} className='text-decoration-none'>
                            <div>
                                <div className='d-flex justify-content-end px-1 border-bottom border-light pt-1'>
                                    <p className='text-danger'>Get 14% off</p>
                                    <i className="bi bi-brightness-low-fill text-danger"></i>
                                </div>
                                <img src="https://www.bigbasket.com/media/uploads/p/l/126906_8-aashirvaad-atta-whole-wheat.jpg?tr=w-384,q=80" className="w-75 h-25 " alt="item" />
                                <div className='text-start'>
                                    <span className='text-secondary'>Fresho</span>
                                    <p className='text-dark'>Aashirvaad Atta/Ghau Peeth - Whole Wheat, 10 kg Pouch</p>
                                    <select className="form-select" aria-label="Default select example">
                                        <option defaultValue>10kg-Rs.460.8</option>
                                    </select>
                                    <div className='pricing-container pb-2'>
                                        <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.534</s><span className='text-dark'>Rs.460.8</span></p>
                                        <p className='text-secondary text-wrap'><i className="fa-solid fa-truck ms-2 me-2"></i>Standard Deliver: 11 sep, 3PM to 7PM</p>
                                    </div>
                                    <button className='btn btn-danger btn-sm opacity-75 mt-2 ms-3'>View Product</button>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className='col-lg-3 col-md-4 col-6 border border-secondary border-opacity-25 rounded pb-2'>
                        <Link to={`/viewproduct/2`} className='text-decoration-none'>
                            <div>
                                <div className='d-flex justify-content-end px-1 border-bottom border-light pt-1'>
                                    <p className='text-danger'>Get 1% off</p>
                                    <i className="bi bi-brightness-low-fill text-danger"></i>
                                </div>
                                <img src="https://www.bigbasket.com/media/uploads/p/l/241600_5-tata-salt-iodized.jpg?tr=w-384,q=80" className="w-75 h-25 " alt="item" />
                                <div className='text-start'>
                                    <span className='text-secondary'>Fresho</span>
                                    <p className='mb-4 pb-3 text-dark'>Tata Salt Iodized, 1 kg Pouch</p>
                                    <select className="form-select" aria-label="Default select example">
                                        <option defaultValue>1kg-Rs.27.72</option>
                                    </select>
                                    <div className='pricing-container pb-2'>
                                        <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.28</s><span className='text-dark'>Rs.27.72</span></p>
                                        <p className='text-secondary text-wrap'><i className="fa-solid fa-truck ms-2 me-2"></i>Standard Deliver: 11 sep, 3PM to 7PM</p>
                                    </div>
                                    <button className='btn btn-danger btn-sm opacity-75 ms-3 mt-2'>View Product</button>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className='col-lg-3 col-md-4 col-6 border border-secondary border-opacity-25 rounded pb-2'>
                        <Link to={`/viewproduct/3`} className='text-decoration-none'>
                            <div>
                                <div className='d-flex justify-content-end px-1 border-bottom border-light pt-1'>
                                    <p className='text-danger'>Get 5% off</p>
                                    <i className="bi bi-brightness-low-fill text-danger"></i>
                                </div>
                                <img src="https://www.bigbasket.com/media/uploads/p/l/104851_4-amul-masti-dahi.jpg?tr=w-384,q=80" className="w-75 h-25 " alt="item" />
                                <div className='text-start'>
                                    <span className='text-secondary'>Fresho</span>
                                    <p className='mb-4 pb-3 text-dark'>Amul Masti Dahi, 400 g Cup</p>
                                    <select className="form-select" aria-label="Default select example">
                                        <option defaultValue>400g-Rs.40</option>
                                    </select>
                                    <div className='pricing-container pb-2'>
                                        <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.42</s><span className='text-dark'>Rs.40</span></p>
                                        <p className='text-secondary text-wrap'><i className="fa-solid fa-truck ms-2 me-2"></i>Standard Deliver: 11 sep, 3PM to 7PM</p>
                                    </div>
                                    <button className='btn btn-danger btn-sm opacity-75 ms-3 mt-2'>View Product</button>
                                </div>
                            </div>
                        </Link>
                    </div>
                    <div className='col-lg-3 col-6 d-lg-block d-md-none border border-secondary border-opacity-25 rounded pb-2'>
                        <Link to={`/viewproduct/4`} className='text-decoration-none'>
                            <div>
                                <div className='d-flex justify-content-end px-1 border-bottom border-light pt-1'>
                                    <p className='text-danger'>Get 12% off</p>
                                    <i className="bi bi-brightness-low-fill text-danger"></i>
                                </div>
                                <img src="https://www.bigbasket.com/media/uploads/p/l/214431_11-madhur-sugar-refined.jpg?tr=w-384,q=80" className="w-75 h-25 " alt="item" />
                                <div className='text-start'>
                                    <span className='text-secondary'>Fresho</span>
                                    <p className='text-dark'>Madhur Sugar/Sakhar - Refined, 5 kg Pouch</p>
                                    <select className="form-select" aria-label="Default select example">
                                        <option defaultValue>5kg-Rs.260</option>
                                    </select>
                                    <div className='pricing-container pb-2'>
                                        <p className='mb-0 fs-6 text-secondary'>MRP:<s className='fs-6'>Rs.295</s><span className='text-dark'>Rs.260</span></p>
                                        <p className='text-secondary text-wrap'><i className="fa-solid fa-truck ms-2 me-2"></i>Standard Deliver: 11 sep, 3PM to 7PM</p>
                                    </div>
                                    <button className='btn btn-danger btn-sm opacity-75 mt-2 ms-3'>View Product</button>
                                </div>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

// const mapStateToProps = (state) => {
//     return {
//         ProductsData: state.ProductsData,
//         cartItems: state.cartItems
//     }
//   }


export default Offers