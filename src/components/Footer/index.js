import React, { Component } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faWonSign} from '@fortawesome/free-solid-svg-icons'

export class Footer extends Component {
  render() {
    return (
      <div className='container'>
        <div className='row ps-1'>
          <div className='col-md-12 border border-secondary text-start py-4 px-3 mt-5' style={{ fontFamily: 'Roboto' }}>
            <h3>bigbasket – online grocery store</h3>
            <p className='text-wrap opacity-75'>Did you ever imagine that the freshest of <span className='fw-bold' style={{ color: '#07ab33' }}>fruits and vegetables</span>, top quality pulses and food grains, <span className='fw-bold' style={{ color: '#07ab33' }}>dairy products</span> and hundreds of branded items could be handpicked and delivered to your home, all at the click of a button? India’s first comprehensive online megastore, bigbasket.com, brings a whopping 20000+ products with more than 1000 brands, to over 4 million happy customers. From household cleaning products to beauty and makeup, bigbasket has everything you need for your daily needs. bigbasket.com is convenience personified We’ve taken away all the stress associated with shopping for daily essentials, and you can now order all your household products and even buy groceries online without travelling long distances or standing in serpentine queues. Add to this the convenience of finding all your requirements at one single source, along with great savings, and you will realize that bigbasket- India’s largest online supermarket, has revolutionized the way India shops for groceries. Online grocery shopping has never been easier. Need things fresh? Whether it’s fruits and vegetables or dairy and meat, we have this covered as well! Get fresh eggs, meat, fish and more online at your convenience. Hassle-free Home Delivery options</p>
            <p className='opacity-75'>We deliver to 25 cities across India and maintain excellent delivery times, ensuring that all your products from groceries to snacks <span className='fw-bold' style={{ color: '#07ab33' }}>branded foods</span> reach you in time.</p>
            <ul>
              <li>Slotted Delivery: Pick the most convenient delivery slot to have your grocery delivered. From early morning delivery for early birds, to late-night delivery for people who work the late shift, bigbasket caters to every schedule.</li>
              <li>Express Delivery: This super useful service can be availed by customers in cities like Bangalore, Mumbai, Pune, Chennai, Kolkata, Hyderabad and Delhi-NCR in which we deliver your orders to your doorstep in 90 Minutes.</li>
              <li>BB Specialty stores: Missed out on buying that essential item from your favorite neighborhood store for tonight’s party? We’ll deliver it for you! From bakery, sweets and meat to flowers and chocolates, we deliver your order in 90 minutes, through a special arrangement with a nearby specialty store, verified by us.</li>
            </ul>
            <button className='border border-1 border-secondary'>ReadMore..</button>
          </div>

          <div className='col-lg-3 col-md-6 col-12 text-start px-lg-3 ps-5 mt-5'>
            <p className='fs-5' style={{ color: '#7ad454' }}>bigbasket</p>
            <ul className='list-unstyled opacity-75'>
              <li className='lh-lg'>About Us</li>
              <li className='lh-lg'>In News</li>
              <li className='lh-lg'>Green bigbasket</li>
              <li className='lh-lg'>Privacy Policy</li>
              <li className='lh-lg'>Affiliate</li>
              <li className='lh-lg'>Terms and Conditions</li>
              <li className='lh-lg'>Careers At bigbasket</li>
              <li className='lh-lg'>bb Instant</li>
              <li className='lh-lg'>bb Daily</li>
              <li className='lh-lg'>bb Blog</li>
              <li className='lh-lg'>bbnow</li>
            </ul>
          </div>

          <div className='col-lg-3 col-md-6 text-start px-lg-3 ps-5 mt-5'>
            <p className='fs-5' style={{ color: '#7ad454' }}>Help</p>
            <ul className='list-unstyled opacity-75'>
              <li className='lh-lg'>FAQs</li>
              <li className='lh-lg'>Contact Us</li>
              <li className='lh-lg'>bb Wallet FAQs</li>
              <li className='lh-lg'>bb Wallet T&Cs</li>
              <li className='lh-lg'>Vendor Connect</li>
            </ul>
          </div>

          <div className='col-lg-3 col-md-6 text-start px-lg-3 ps-5 mt-5'>
            <p className='fs-5' style={{ color: '#7ad454' }}>Download Our App</p>
            <div className='d-flex flex-column justify-content-start'>
              <img src='https://www.bbassets.com/static/v2584/custPage/build/content/img/Google-App-store-icon.png' alt='Googlestore' className='w-50 mb-2' />
              <img src='https://www.bbassets.com/static/v2584/custPage/build/content/img/Apple-App-store-icon.png' alt='applestore' className='w-50 mb-2' />
            </div>
          </div>

          <div className='col-lg-3 col-md-6 text-start px-lg-3 ps-5 mt-5'>
            <p className='fs-5' style={{ color: '#7ad454' }}>Follow Us</p>
            <div className='d-flex'>
              {/* <i className="fa-brands fa-facebook fs-1 me-2" style={{ color: '#4267B2' }}></i> */}
              {/* <FontAwesomeIcon icon="fa-brands fa-facebook" className="fs-1 me-2" style={{ color: '#4267B2' }} /> */}
              <FontAwesomeIcon icon={faWonSign} className='fs-1 me-2' style={{ color: '#E60023' }} />
              {/* <i className="fa-brands fa-pinterest fs-1 me-2" style={{ color: '#E60023' }}></i>
              <i className="fa-brands fa-twitter fs-1 me-2" style={{ color: '#1DA1F2' }}></i>
              <i className="fa-brands fa-instagram fs-1 me-2" style={{ color: '#e95950' }}></i> */}
            </div>
          </div>

          <div className='col-lg-12 mt-5'>
            <hr />
            <div className='text-start'>
              <dl className="row">
                <dt className="col-sm-3 col-lg-2" style={{ color: '#7ad454' }}>POPULAR CATEGORIES:</dt>
                <dd className="col-sm-9 col-lg-10 opacity-75">
                  Sunflower Oils, Wheat Atta, Ghee, Milk, Health Drinks, Flakes, Organic F&V, Namkeen, Eggs, Floor Cleaners, Other Juices, Leafy Vegetables, Frozen Veg Food, Diapers & Wipes,</dd>

                <dt className="col-sm-3 col-lg-2" style={{ color: '#7ad454' }}>POPULAR BRANDS:</dt>
                <dd className="col-sm-9 col-lg-10 opacity-75">
                  <p>Amul, Nescafe , MTR, RED BULL , elite cake, Pediasure, Yummiez, Yera, Yakult, Britannia, Wow Momo, Fortune , Haldirams , Ferrero, Lays, Patanjali, McCain, kwality walls, Cadbury Dairy Milk, Pedigree,</p>
                </dd>

                <dt className="col-sm-3 col-lg-2" style={{ color: '#7ad454' }}>CITIES WE SERVE:</dt>
                <dd className="col-sm-9 col-lg-10 opacity-75">Bangalore, Hyderabad, Mumbai, Pune, Chennai, Delhi, Mysore, Madurai, Coimbatore, Vijayawada-Guntur, Kolkata, Ahmedabad-Gandhinagar, Nashik Business, Lucknow-Kanpur, Gurgaon, Vadodara, Visakhapatnam, Surat, Nagpur, Patna, Indore, Chandigarh Tricity, Jaipur, Bhopal, Noida-Ghaziabad, Kochi, Krishna District, Bhubaneshwar-Cuttack, Guwahati, Renigunta, Hubli, Davanagere, Trichy, Amravati, Raipur, Rajkot, Gwalior, Bareilly, Allahabad, Hyderabad Rural, Bangalore Rural, Chennai Rural, Vizag Rural, Lucknow Rural, Noida Rural, Ahmedabad Rural, Bhopal Rural, Bhubaneswar Rural, Coimbatore Rural, Chandigarh Rural, Gurugram Rural, Guwahati Rural, Indore Rural, Kochi Rural, Kolkata Rural, Mumbai Rural, Mysore Rural, Nagpur Rural, Patna Rural, Pune Rural, Surat Rural, Vadodara Rural, Jaipur Rural, Ranchi, Nashik, Agra, </dd>

                <dt className="col-sm-3 col-lg-2 text-truncate" style={{ color: '#7ad454' }}>PAYMENTS:</dt>
                <dd className="col-sm-9 col-lg-10 opacity-75">CASH ON DELIVERY
                  <div className='d-flex'>
                    <img src='https://www.freepnglogos.com/uploads/visa-and-mastercard-logo-26.png' alt='payment' className='w-25 me-2' />
                    <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Paytm_payments_bank.svg/1280px-Paytm_payments_bank.svg.png' alt='payment' className='w-25 h-25 pt-3 ms-2 me-2' />
                    <img src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTb1qHeivylJAnG_DIDciYrRmz3r0wPP2AHdQ&usqp=CAU' alt='payment' className='w-25 pb-2'/>
                  </div>
                </dd>
              </dl>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Footer