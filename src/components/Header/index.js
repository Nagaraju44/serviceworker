import React, { Component } from 'react'
import { connect } from 'react-redux'
import logo from './Big-Basket.svg'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBasketShopping } from '@fortawesome/free-solid-svg-icons'

export class Header extends Component {
    render() {
        return (
            <div className='container-fluid bg-light sticky-top'>
                <div className='row'>
                    <div className='col-3 col-md-2 mt-1 d-flex flex-row justify-content-md-end justify-content-center pe-0'>
                        <Link to='/'><img src={logo} alt={"BigBasketLogo"}className="w-50 img-thumbnail border border-white pt-2" /></Link>
                    </div>
                    <div className='col-6 col-md-6 ps-0 d-flex align-items-center mx-md-0'>
                        <nav className="navbar bg-white ms-0 w-100">
                            <div className="container-fluid ps-0">
                                <form className="d-flex w-100" role="search">
                                    <input className="form-control w-100" type="search" placeholder="Search products" aria-label="Search" />
                                    <button className="btn btn-success border border-0" type="submit" style={{ backgroundColor: '#84c225' }}><i className="fa-solid fa-magnifying-glass"></i></button>
                                </form>
                            </div>
                        </nav>
                    </div>
                    <div className='col-2 d-md-none pt-2 text-start'>
                        <Link to='/myorders'>
                            <div className='btn btn-sm btn-success mt-3 border border-0' style={{ backgroundColor: '#4c6020' }}>My Orders</div>
                        </Link>
                    </div>

                    <div className='col-lg-2 col-md-3 d-md-flex justify-content-center align-items-center d-none'>
                        <Link to='/cartpage' className='text-decoration-none'>
                            <div className='d-flex align-items-center  h-50 bg-light px-3 shadow-sm py-0'>
                                {/* <i className="fa-solid fa-basket-shopping "></i> */}
                                <FontAwesomeIcon icon={faBasketShopping} className='text-danger fs-1 pe-2' />
                                <p className='text-secondary pt-2'> <span className='text-danger fs-4'>{this.props.cartItems.length}</span> items</p>
                            </div>
                        </Link>
                    </div>
                    <div className='col-5 col-md-3 d-flex align-items-start justify-content-md-end pt-0 mt-0'>
                        <div className="btn-group">
                            {/* <button className='btn btn-success btn-sm background-opacity-25'>Your  Orders</button> */}
                            <button type="button" className="btn btn-success dropdown-toggle border border-0" style={{ backgroundColor: '#84c225' }} data-bs-toggle="dropdown" aria-expanded="true">
                                SHOP BY CATEGORY
                            </button>
                            <ul className="dropdown-menu dropdown-menu-end">
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Fruits & Vegetables</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Bakery, Cakes & Dairy</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Beverages</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Food Grails, Oil, Masala</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Snacks & Branded Foods</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Beauty & Hygiene</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Cleaning & HouseHold</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Kitchen, Garden & Pets</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Eggs, Meat & Fish</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Gourmet & World Foods</button></li>
                                <li><button className="dropdown-item text-secondary border-bottom border-secondary border-opacity-25" type="button">Baby Care</button></li>
                                <li><button className="dropdown-item text-secondary" type="button">View All</button></li>
                            </ul>
                        </div>
                    </div>

                    <div className='col-4 col-lg-6 col-md-8 d-none d-md-block border border-secondary border-opacity-10 pt-0 mt-0'>
                        <div className='d-flex flex-row align-items-start'>
                            <i className="fa-solid fa-tag text-danger me-2 pt-2"></i>
                            <p className='pt-2'>Offers</p>
                            <Link to='/myorders'>
                                <div className='btn btn-sm btn-success ms-3 mt-1 border border-0' style={{ backgroundColor: '#4c6020' }}>My Orders</div>
                            </Link>
                        </div>
                    </div>
                    <div className='col-7 d-md-none d-flex justify-content-end'>
                        <Link to='/cartpage' className='text-decoration-none'>
                            <div className='d-flex align-items-center  h-75 bg-light px-3 shadow-sm py-0 sticky-top'>
                                <i className="fa-solid fa-basket-shopping text-danger fs-1 pe-2"></i>
                                <p className='text-secondary pt-2'><span className='text-danger fs-4'>{this.props.cartItems.length}</span> items</p>
                            </div>
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        ProductsData: state.ProductsData,
        cartItems: state.cartItems
    }
}

export default connect(mapStateToProps, null)(Header)